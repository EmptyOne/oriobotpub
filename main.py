import time
import telebot
from telebot import types
import oriosql as db
import orioparser as prs
import orioschedule as sch

bot = telebot.TeleBot(<token>) 

active_users = [] # зашедшие пользователи
login_users = [] # в процессе входа
reg_users = [] # в процессе регистрации
parsing_users = [] # готовятся парсить предмет
sch_users = {} # готовятся получить расписание

class TeleUser:
    """struct user info for the bot"""
    __id__ = 0
    __pin__ = ""
    __ologin__ = ""
    __opass__  = ""
    __group__  = ""
    """methods"""
    def __init__(self, id, login, password, group, pin): # constructor
        self.__id__ = id
        self.__ologin__ = login
        self.__opass__ = password
        self.__group__ = group
        self.__pin__ = pin
    def get_id(self):
        return self.__id__
    def get_pin(self):
        return self.__pin__
    def get_lp(self):
        return [ self.__ologin__ , self.__opass__ ] # return list
    def get_grp(self):
        return self.__group__
    def get_lpg(self):
        return [ self.__ologin__ , self.__opass__ , self.__group__ ]

############################# UTILITY FUNCS #######################################
def is_auth(message): # auth check
    flag = False
    for obj in active_users: # whether user id in object list
        if obj.get_id() == message.from_user.id:
            flag = True
    if flag: 
        return True
    else:
        return False

def is_auth_msg(message): # auth check with warning message
    flag = False
    for obj in active_users: # whether user id in object list
        if obj.get_id() == message.from_user.id:
            flag = True
    if flag: 
        return True
    else:
        bot.send_message(message.from_user.id, "Отказано в доступе -> /login")
        return False

def lp_from_list(id): # get login and password from object list
    flag = False
    for obj in active_users: 
        if obj.get_id() == id:
            flag = True
            return obj.get_lp()
    if not flag:  return ['error', 'error']

def lpg_from_list(id): # get login, password and group from object list
    flag = False
    for obj in active_users: 
        if obj.get_id() == id:
            flag = True
            return obj.get_lpg()
    if not flag:  return ['error', 'error', 'error']

def grp_from_list(id): # get group from object list
    flag = False
    for obj in active_users: 
        if obj.get_id() == id:
            flag = True
            return obj.get_grp()
    if not flag:  return 'error'

############################# COMMANDS DECORATORS #######################################
@bot.message_handler(commands=['help', 'start'])
def handle_help(message):
    if is_auth(message):
        bot.send_message(message.from_user.id, "Попробуй посмотреть баллы -> /marks")
    elif message.from_user.id in login_users:
        bot.send_message(message.from_user.id, "Закончите вход!")
    elif message.from_user.id in reg_users:
        bot.send_message(message.from_user.id, "Продолжайте регистрацию!")
    else:
        bot.send_message(message.from_user.id, "Зарегистрируйтесь -> /reg")

@bot.message_handler(commands=['info']) # инфо по пользователю
def handle_info(message):
    try:
        if is_auth_msg(message):
            [l, p, g] = lpg_from_list(message.from_user.id)
            info_data = prs.parse_info(l,p)
            bot.send_message(message.from_user.id, info_data)
    except: bot.send_message(message.from_user.id, "Произошла ошибка :(")

########### SCHEDULE FEATURES ###########
@bot.message_handler(commands=['weekshort']) # краткое расписание
def handle_weekshort(message):
    try:
        if is_auth_msg(message) and not message.from_user.id in sch_users:
            sch_users[message.from_user.id] = 'short'
            markup =  types.ReplyKeyboardMarkup(row_width=2) #types.ReplyKeyboardRemove()
            btn_1ch = types.KeyboardButton('1числ')
            btn_1zn = types.KeyboardButton('1знам')
            btn_2ch = types.KeyboardButton('2числ')
            btn_2zn = types.KeyboardButton('2знам')
            btn_cur = types.KeyboardButton('на текущую неделю')
            markup.add(btn_1ch, btn_1zn)
            markup.add(btn_2ch, btn_2zn)
            markup.add(btn_cur)
            bot.send_message(message.from_user.id, "На какую неделю показать расписание?", reply_markup=markup)
    except: bot.send_message(message.from_user.id, "Произошла ошибка :(")

@bot.message_handler(commands=['weekfull']) # подробное расписание
def handle_weekfull(message):
    try:
        if is_auth_msg(message) and not message.from_user.id in sch_users:
            sch_users[message.from_user.id] = 'full'
            markup =  types.ReplyKeyboardMarkup(row_width=2) 
            btn_1ch = types.KeyboardButton('1числ')
            btn_1zn = types.KeyboardButton('1знам')
            btn_2ch = types.KeyboardButton('2числ')
            btn_2zn = types.KeyboardButton('2знам')
            btn_cur = types.KeyboardButton('на текущую неделю')
            markup.add(btn_1ch, btn_1zn)
            markup.add(btn_2ch, btn_2zn)
            markup.add(btn_cur)
            bot.send_message(message.from_user.id, "На какую неделю показать подробное расписание?", reply_markup=markup)
    except: bot.send_message(message.from_user.id, "Произошла ошибка :(")

@bot.message_handler(commands=['today']) # расписание на сегодня
def handle_today(message):
    try:
        if is_auth_msg(message):
            [l,p,g] = lpg_from_list(message.from_user.id)
            res = sch.get_rasp_today(l,p,g)
            #res = "tdy"
            bot.send_message(message.from_user.id, res)        
    except: bot.send_message(message.from_user.id, "Произошла ошибка :(")

@bot.message_handler(commands=['tomorrow']) # расписание на завтра
def handle_tomorrow(message):
    try:
        if is_auth_msg(message):
            [l,p,g] = lpg_from_list(message.from_user.id)
            res = sch.get_rasp_tomorrow(l,p,g)
            #res= "tmrw"
            bot.send_message(message.from_user.id, res)        
    except: bot.send_message(message.from_user.id, "Произошла ошибка :(")

@bot.message_handler(commands=['now']) # пара сейчас
def handle_classnow(message):
    try:
        if is_auth_msg(message):
            [l,p,g] = lpg_from_list(message.from_user.id)
            res = sch.get_rasp_now(l,p,g)
            #res= "RUN!"
            bot.send_message(message.from_user.id, res)        
    except: bot.send_message(message.from_user.id, "Произошла ошибка :(")

########### SUBJECTS FEATURES ###########
@bot.message_handler(commands=['subjects']) # выгрузка инфы по предметам
def handle_subjects(message):
    try:
        if is_auth_msg(message):
            [l, p] = lp_from_list(message.from_user.id)
            sub_data = prs.parse_subjects(l,p)
            if isinstance(sub_data, list):
                for rows in sub_data: 
                    bot.send_message(message.from_user.id, rows)
                    time.sleep(0.5)
            else: bot.send_message(message.from_user.id, sub_data)
    except: bot.send_message(message.from_user.id, "Произошла ошибка или бот перегружен!") 

@bot.message_handler(commands=['marks']) # выгрузка успеваемости по предметам
def handle_marks(message):
    try:
        if is_auth_msg(message):
            [l, p] = lp_from_list(message.from_user.id)
            info_data = prs.parse_marks(l,p)
            if len(info_data) > 2500:
                bot.send_message(message.from_user.id, info_data[:2500])
                bot.send_message(message.from_user.id, info_data[2501:])
            else:
                bot.send_message(message.from_user.id, info_data)
    except: bot.send_message(message.from_user.id, "Произошла ошибка :(")     

@bot.message_handler(commands=['getmark']) # баллы по предмету
def handle_info(message):
    try:
        if is_auth(message) and not message.from_user.id in parsing_users:
            parsing_users.append(message.from_user.id)        
            [l, p] = lp_from_list(message.from_user.id)   
            user_subjects = prs.parse_subjects_list(l,p)
            bot.send_message(message.from_user.id, user_subjects)
        else:
            bot.send_message(message.from_user.id, "Что-то вы делаете не так... /login")        
    except: bot.send_message(message.from_user.id, "Произошла ошибка :(")

############################# AUTH ROUTINE #######################################

@bot.message_handler(commands=['login'])
def handle_command_login(message):
    if not is_auth(message) and not message.from_user.id in login_users and not message.from_user.id in reg_users:
        bot.send_message(message.from_user.id, "Введите код (4 цифры) для входа")
        login_users.append(message.from_user.id)
    else: bot.send_message(message.from_user.id, "Вы уже вошли или в процессе регистрации/входа")

@bot.message_handler(commands=['quit'])
def handle_command_login(message):
    if is_auth(message): 
        for obj in active_users:
            if obj.get_id() == message.from_user.id:
                active_users.remove(obj)
        bot.send_message(message.from_user.id, "Вы вышли из системы.")
    else:
        bot.send_message(message.from_user.id, "Вы не вошли. /login")

@bot.message_handler(commands=['del'])
def handle_command_login(message):
    try:
        if is_auth(message):
            pincode = ''
            for obj in active_users:
                if obj.get_id() == message.from_user.id: pincode = obj.get_pin()
            ok_result = db.delete_user(message.from_user.id, pincode)
            if ok_result:
                for obj in active_users:
                    if obj.get_id() == message.from_user.id:
                        active_users.remove(obj)
                        break
                bot.send_message(message.from_user.id, "Пользователь удален!")
            else: bot.send_message(message.from_user.id, "Не удалось удалить пользователя!")
    except: bot.send_message(message.from_user.id, "Произошла ошибка :(")      

@bot.message_handler(commands=['reg']) 
def handle_command_login(message):
    if not is_auth(message) and not message.from_user.id in login_users and not message.from_user.id in reg_users:
        bot.send_message(message.from_user.id, "ВНИМАНИЕ!\n" +
                                                "Это тестовая версия бота\n" +
                                                "Возможны 'разлогины' и временные вылеты\n" +
                                                "Обо всех найденных ошибках сообщайте:\n" +
                                                "mail@mail.ru\n" +
                                                "=======================\n" +
                                                "Введите ЛОГИН ОТ ОРИОКСА, ПАРОЛЬ ОТ ОРИОКСА,\n" +     
                                                "и произвольный ПИНКОД (4 цифры) для быстрого входа\n" +
                                                "ЧЕРЕЗ ЗНАК '*'")
        reg_users.append(message.from_user.id)
    else: 
        bot.send_message(message.from_user.id, "Вы уже зарегистрированы или регистрируетесь")
        
############################# TEXT ROUTINE #######################################

@bot.message_handler(content_types=["text"])
def handle_text(message):
    if message.text.lower() == "техпрог":
        bot.send_message(message.from_user.id, 'лучший ^_^')
    elif message.text.lower() == "жозе алехандро":
        bot.send_message(message.from_user.id, 'Ну ты понял... ХЛЕБ')
        audio = open('NuTyPonyal.mp3', 'rb')
        bot.send_audio(message.from_user.id, audio)
    elif message.text.lower() == "orio":
        msg = "...../----/  /----/  /-/-/  /----/\n" \
            + "..../     / /     /   /    /     /.\n" \
            + ".../     / /----/    /    /     /..\n" \
            + "../     / /\        /    /     /...\n" \
            + "./     / /  \      /    /     /....\n" \
            + "/----/  /    \  /-/-/  /----/.....\n"
        bot.send_message(message.from_user.id, msg)                               
    elif message.from_user.id in reg_users: # REGISTRATION
        try:
            if not is_auth(message) and not message.from_user.id in login_users:
                fields = message.text.split('*')
                ugrp = prs.parse_group(str(fields[0]), str(fields[1]))
                db.add_to_table(message.from_user.id, fields[0], fields[1], fields[2], ugrp)
                bot.send_message(message.from_user.id, "Теперь можно войти -> /login")
                reg_users.remove(message.from_user.id)
            else: bot.send_message(message.from_user.id, "Вы уже зарегистрированы. Войдите -> /login")
        except: 
            bot.send_message(message.from_user.id, "Неверный формат сообщения! Начните регистрацию снова -> /reg")
            reg_users.remove(message.from_user.id)
    elif message.from_user.id in login_users: # LOGIN
        try:
            data = db.get_user(message.from_user.id, message.text)
            if data[0] != 'NoLogin' and data[1] != 'NoPassword' and data[2] != 'NoGroup':
                login_users.remove(message.from_user.id)
                active_users.append(TeleUser( message.from_user.id, data[0], data[1], data[2], str(message.text) ))
                bot.send_message(message.from_user.id, "Успешный вход! Посмотрите свои баллы -> /marks")
                print('BOT: Active users: '); print(len(active_users))
            else:
                bot.send_message(message.from_user.id, "Пользователь не найден! Попробуйте снова войти -> /login")
                login_users.remove(message.from_user.id)
        except:
            bot.send_message(message.from_user.id, "Ошибка входа! Попробуйте снова -> /login")
            login_users.remove(message.from_user.id)
    elif is_auth(message) and message.from_user.id in parsing_users: # parse subject
        [l, p, g] = lpg_from_list(message.from_user.id)
        if l != 'error' and p != 'error' and g != 'error':
            sublist = prs.parse_subjects_list(l,p)
            try:
                number = int(message.text)
                answer = prs.parse_subject_marks(l, p, number)
                bot.send_message(message.from_user.id, answer)
                parsing_users.remove(message.from_user.id)
            except:
                bot.send_message(message.from_user.id, "Неверный формат сообщений, попробуйте снова -> /getmark") 
                parsing_users.remove(message.from_user.id)
    elif is_auth(message) and message.from_user.id in sch_users: # SCHEDULE
        [l, p, g] = lpg_from_list(message.from_user.id)
        weektype = sch_users[message.from_user.id]
        try:
            msg = sch.get_rasp_week(l, p, g, message.text, weektype)
            #msg = "Кек"
            markup = types.ReplyKeyboardRemove(selective=False) # remove keyboard
            bot.send_message(message.from_user.id, msg, reply_markup=markup)
            del sch_users[message.from_user.id]
        except:
            markup = types.ReplyKeyboardRemove(selective=False)
            bot.send_message(message.from_user.id, "Ошибка при получении расписания, попробуйте снова", reply_markup=markup)
            del sch_users[message.from_user.id]
                
    else:
        bot.send_message(message.from_user.id, "Попробуйте -> /help")

############################# MAIN LOOP #######################################
while True:
    try:
        print('BOT: Start polling...')
        bot.polling(none_stop=True)        
    except:
        print('BOT: Polling error occured. Timeout 5s')
        time.sleep(5)



