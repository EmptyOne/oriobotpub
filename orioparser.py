import json
import requests
from lxml import html

CSRF = '_csrf'
LOGINFORM = 'LoginForm[login]'
PASSFORM = 'LoginForm[password]'
RMFORM = 'LoginForm[rememberMe]'

login_url = 'https://orioks.miet.ru/main/login'
page_url = 'https://orioks.miet.ru/student/student'
group_url = 'https://orioks.miet.ru/main/profile'

def parse_week(user_login, user_password):
    ''' Return string like 4week/2znam or return False '''
    try:
        session_requests = requests.session()
        result = session_requests.get(login_url)
        tree = html.fromstring(result.text)
        auth_token = list(set(tree.xpath("//input[@name='_csrf']/@value")))[0]

        payload = {
            CSRF: auth_token,
            LOGINFORM: user_login,
            PASSFORM: user_password,
            RMFORM: '0'     
        }

        result = session_requests.post(
            login_url, 
            data = payload, 
            headers = dict(referer=login_url)
        )

        result = session_requests.get(
            group_url, 
            headers = dict(referer = group_url)
        )
        grp_tree = html.fromstring(result.content)
        grp_value = grp_tree.xpath("//li[@class='active']/a")[0].text_content()
        answer = grp_value.split(' ')
        result.close()
        return answer[1] + answer[2] + '/' + answer[3] + answer[4]
    except:
        print("$$ PARSER: can't parse current week!")
        return False

def parse_group(user_login, user_password):
    ''' Parse student group from orioks '''
    try:
        session_requests = requests.session()
        result = session_requests.get(login_url)
        tree = html.fromstring(result.text)
        auth_token = list(set(tree.xpath("//input[@name='_csrf']/@value")))[0]

        payload = {
            CSRF: auth_token,
            LOGINFORM: user_login,
            PASSFORM: user_password,
            RMFORM: '0'     
        }

        result = session_requests.post(
            login_url, 
            data = payload, 
            headers = dict(referer=login_url)
        )

        result = session_requests.get(
            group_url, 
            headers = dict(referer = group_url)
        )
        grp_tree = html.fromstring(result.content)
        grp_value = grp_tree.xpath("//table[@class='table table-user-information']/tbody/tr/td")[1].text_content()
        answer = grp_value.split(' ')
        result.close()
        return answer[0] + ' ' + answer[1]
    except:
        print("$$ PARSER: can't parse student group!")
        return False

def parse_cabinet(user_login, user_password):
    try:
        session_requests = requests.session()
        result = session_requests.get(login_url)
        tree = html.fromstring(result.text)
        auth_token = list(set(tree.xpath("//input[@name='_csrf']/@value")))[0]

        payload = {
            CSRF: auth_token,
            LOGINFORM: user_login,
            PASSFORM: user_password,
            RMFORM: '0'     
        }

        result = session_requests.post(
            login_url, 
            data = payload, 
            headers = dict(referer=login_url)
        )

        result = session_requests.get(
            page_url, 
            headers = dict(referer = page_url)
        )
        orio_tree = html.fromstring(result.content)
        orio_week = orio_tree.xpath("//li[@class='active']/a")[0].text_content()[1:]
        orio_fio = orio_tree.xpath("//a[@class='dropdown-toggle']")[4].text_content()
        orio_list = orio_tree.xpath("//div[@id='forang']")[0].text_content()
        json_list = json.loads(orio_list) 
        result.close()
        return [orio_fio, orio_week, json_list]
    except:
        print("$$ PARSER: can't parse orioks")
        return False


def parse_info(user_login, user_password):
    ''' login + password to request info '''
    answer = parse_cabinet(str(user_login), str(user_password))
    group = parse_group(str(user_login), str(user_password))
    if not isinstance(answer, bool):
        fio = answer[0]
        week = answer[1]
        jlist = answer[2] 
        row = fio + '\n' + str(user_login) + '\n' + week + '\n'                
        row += 'ID студента: ' + str(jlist['student']['id']) + '\n'
        row += 'ID группы: ' + str(jlist['student']['id_group']) + '\n'
        row += 'Название группы: ' + str(group) + '\n'
        row += 20 * '*' + '\n'
        row += 'Направление: ' + str(jlist['np']['name']) + '\n'
        row += 'Номер направления: ' + str(jlist['np']['sh']) + '\n'
        row += 'Длительность обучения: ' + str(jlist['np']['nso']) + '\n'
        row += 20 * '*' + '\n'
        row += 'Специальность: ' + str(jlist['op']['name']) + '\n'
        row += 20 * '*' + '\n'
        row += 'Описание специальности: ' + str(jlist['op']['describe']) + '\n'        
        return row        
    else:
        print("$$ PARSER: info parse failed")
        return "Не удалось получить данные!"

def parse_subjects(user_login, user_password):
    ''' login + password to request subjects info '''
    answer = parse_cabinet(str(user_login), str(user_password))
    if not isinstance(answer, bool):
        jlist = answer[2] 
        rowlist = []
        row = 'Информация по предметам ' + str(user_login) + '\n'
        row += 20 * '*' + '\n'
        row += 20 * ' ' + '\n'
        rowlist.append(row)               
        for predmet in jlist['dises']:
            nrow = ''
            nrow += jlist['dises'][str(predmet)]['name'] + '\n'
            nrow += 'ID предмета: ' + str(jlist['dises'][str(predmet)]['id_science']) + '\n'
            lectures = '0' if str(jlist['dises'][str(predmet)]['plan_lecture']) == 'None' else str(jlist['dises'][str(predmet)]['plan_lecture'])
            seminars = '0' if str(jlist['dises'][str(predmet)]['plan_seminar']) == 'None' else str(jlist['dises'][str(predmet)]['plan_seminar'])
            labs = '0' if str(jlist['dises'][str(predmet)]['plan_lab']) == 'None' else str(jlist['dises'][str(predmet)]['plan_lab'])
            zets = '0' if str(jlist['dises'][str(predmet)]['zet']) == 'None' else str(jlist['dises'][str(predmet)]['zet'])
            nrow += 'Лекций: ' + lectures + '\n'
            nrow += 'Семинаров: ' + seminars + '\n'
            nrow += 'Лабораторных: ' + labs + '\n'
            nrow += 'Зет: ' + zets + '\n'
            nrow += 'Форма контроля: ' + str(jlist['dises'][str(predmet)]['formControl']['name']) + '\n'
            nrow += 'Наличие КМ на этой неделе: '
            if str(jlist['dises'][str(predmet)]['now_km']) == 'True': nrow += 'ДА' + '\n'
            else: nrow += 'НЕТ' + '\n'
            nrow += 20 * '*' + '\n'
            nrow += 'Кафедра: ' + str(jlist['dises'][str(predmet)]['science']['kaf']['sh']) + '\n'
            nrow += 'Ссылка: ' + str(jlist['dises'][str(predmet)]['science']['kaf']['link']) + '\n'
            nrow += 'Телефон: ' + str(jlist['dises'][str(predmet)]['science']['kaf']['phone']) + '\n'
            nrow += 'Почта: ' + str(jlist['dises'][str(predmet)]['science']['kaf']['email']) + '\n'
            nrow += 20 * '*' + '\n' + 'Преподаватели:' + '\n'
            for user in jlist['dises'][str(predmet)]['preps']: nrow += user['name'] + ' (' + user['login'] + ')' + '\n'
            nrow += 20 * '*' + '\n'  
            nrow += 20 * ' ' + '\n'
            rowlist.append(nrow)                     
        return rowlist        
    else:
        print("$$ PARSER: subjects info parse failed")
        return "Не удалось получить данные!"

def parse_marks(user_login, user_password):
    ''' login + password to request marks '''
    answer = parse_cabinet(str(user_login), str(user_password))
    if not isinstance(answer, bool):
        fio = answer[0]
        week = answer[1]
        jlist = answer[2] 
        row = fio + ' ' + str(user_login) + '\n' + week + '\n'
        row += 20 * '*' + '\n'  
        for predmet in jlist['dises']:
            row += jlist['dises'][str(predmet)]['name'] + '\n'
            row += 'баллы: '
            row += str(jlist['dises'][str(predmet)]['grade']['b']) + '/' + \
                   str(jlist['dises'][str(predmet)]['mvb']) + '\n' + \
                   str("сумма | процент | оценка") + '\n' + \
                   str(jlist['dises'][str(predmet)]['grade']['f']) + ' | ' + \
                   str(jlist['dises'][str(predmet)]['grade']['p']) + '% | "' + \
                   str(jlist['dises'][str(predmet)]['grade']['o']) + '"\n'
            row += 20 * '*' + '\n'                       
        return row        
    else:
        print("$$ PARSER: marks parse failed")
        return "Не удалось получить данные!"

def parse_subject_marks(user_login, user_password, subject):
    ''' login + password to request subject's marks '''
    answer = parse_cabinet(str(user_login), str(user_password))
    if not isinstance(answer, bool):
        row = 'Не удалось получить данные!'
        jlist = answer[2]
        i = 0       
        for predmet in jlist['dises']:
            i = i + 1
            if i == subject: 
                row = jlist['dises'][str(predmet)]['name'] + '\n'
                row += 20 * '*' + '\n'
                for fields in jlist['dises'][str(predmet)]['disSegments'][0]['allKms']:
                    if str(fields['sh']) != 'None' and str(fields['name']) != 'None':
                        row += str(fields['sh']) + ' ' + str(fields['name']) + '\n' + str(fields['grade']['b']) + '/' + str(fields['max_ball']) + '\n'
                    else:
                        forma = str(jlist['dises'][str(predmet)]['formControl']['name']).upper() 
                        row += forma + '\n' + str(fields['grade']['b']) + '/' + str(fields['max_ball']) + '\n'                                         
                row += 20 * '*' + '\n'
        return row        
    else:
        print("$$ PARSER: marks parse failed")
        return "Не удалось получить данные!"
    
def parse_subjects_list(user_login, user_password):
    ''' login + password to request list of subjects'''
    answer = parse_cabinet(str(user_login), str(user_password))
    if not isinstance(answer, bool):
        jlist = answer[2] 
        row = 'Выберите предмет - отправьте его номер:\n'
        i = 1  
        for predmet in jlist['dises']:
            row += str(i) + ' - ' + str(jlist['dises'][str(predmet)]['name']) + '\n' 
            i = i + 1                      
        return row        
    else:
        print("$$ PARSER: getsubjectslist parse failed")
        return "Не удалось получить список предметов!"

