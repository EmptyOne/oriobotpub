import json
import datetime
import requests
from lxml import html
import orioparser as parser

################## REQUIREMENTS ###################
# CODE STYLE:
# only functions, global vars and test lines
# comments in function header like ''' example '''
# try/except for all funcs
# ANSWER:
# one string var with \t, \n and so on
# if error occured return string like:
# $$ SCH: super fatal error with sch!
###################################################

def get_weektype(week):
    """ Convert week string into int """
    if week == "1числитель":
        return 0
    elif week == "1знаменатель":
        return 1
    elif week == "2числитель":
        return 2
    elif week == "2знаменатель":
        return 3
    else: return 4

def get_dayname(number):
    """ Convert day number to string """
    days = [ "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота" ]
    if number <= 6:
        return days[number - 1]
    else:
        return ""

def get_rasp_week(login, password, group, curweek, viewtype): # TODO: try/catch
    "Get l/g/p + current week + view type AND return string with WEEK schedule"   
    try: 
        get_week = ['1числ','1знам','2числ','2знам','на текущую неделю']
        curweek_id = get_week.index(curweek)
        request_groupname = str(group)
        request_data = {'group': request_groupname}    
        r = requests.post("https://miet.ru/schedule/data", data=request_data)
        jdata = json.loads(r.text) # json
        raw = parser.parse_week(login, password) # raw week string
        if curweek_id == 4:
            week = get_weektype(raw.split('/')[1]) 
        else:
            week = curweek_id      
        weekclass = {}
        for x in jdata['Data']:
            if x['DayNumber'] == week:
                weekclass[ str(x['Day'])+"/"+str(x['Time']['Code']) ] = x  # "day/class"
        answer = "Расписание " + curweek + "\n"    
        if viewtype == "short": 
            answer += "********************\n"
            for day in range(1,7):
                answer += get_dayname(day) + "\n"
                for p in range(10):
                    ckey = str(day) + "/" + str(p)
                    if ckey in weekclass:                   
                        answer += weekclass[ckey]['Time']['Time'] + '|'
                        answer += weekclass[ckey]['Class']['Name'] + '|' 
                        answer += weekclass[ckey]['Class']['Teacher'] + '|'
                        answer += weekclass[ckey]['Room']['Name'] + '\n' 
                answer += "********************\n"                       
        elif viewtype == "full":
            for day in range(1,7):
                answer += '****' + get_dayname(day) + '****' + '\n'
                for p in range(10):
                    ckey = str(day) + "/" + str(p)
                    if ckey in weekclass:                   
                        ftime = weekclass[ckey]['Time']['TimeFrom'].split('T')[1][0:5]
                        ttime = weekclass[ckey]['Time']['TimeTo'].split('T')[1][0:5]
                        answer += weekclass[ckey]['Time']['Time'] + ' с ' + ftime + ' до ' + ttime + '\n'
                        answer += weekclass[ckey]['Class']['Name'] + '\n' 
                        answer += weekclass[ckey]['Class']['TeacherFull'] + '\n'
                        answer += 'Аудитория ' + weekclass[ckey]['Room']['Name'] + '\n'  
                        answer += "===\n"  
        else: answer = "viewtype is incorrect!"
        return answer
    except:
        print('$$ SCH: /week error')
        return 'Ошибка при получении расписния на неделю!'

def get_rasp_today(login, password, group): 
    "Get l/g/p and return string with schedule"
    try:
        request_groupname = str(group)
        request_data = {'group': request_groupname}    
        r = requests.post("https://miet.ru/schedule/data", data=request_data)
        jdata = json.loads(r.text) # json
        raw = parser.parse_week(login, password) # raw week string
        week = get_weektype(raw.split('/')[1])       
        day = datetime.datetime.today().weekday() + 1 # today weekday
        classes = {}
        for x in jdata['Data']:
            if x['Day'] == day and x['DayNumber'] == week:
                classes[ x['Time']['Code'] ] = x
        answer = "Расписание на сегодня\n"
        answer += "********************\n"
        for i in range(10):
            if i in classes:
                ftime = classes[i]['Time']['TimeFrom'].split('T')[1][0:5]
                ttime = classes[i]['Time']['TimeTo'].split('T')[1][0:5]
                answer += classes[i]['Time']['Time'] + ' с ' + ftime + ' до ' + ttime + '\n' 
                answer += classes[i]['Class']['Name'] + '\n' 
                answer += classes[i]['Class']['Teacher'] + '\n'
                answer += 'Аудитория ' + classes[i]['Room']['Name'] + '\n'
                answer += "********************\n"
        return answer 
    except:
        print('$$ SCH: /today error')
        return 'Ошибка при получении расписания на сегодня :('

def get_rasp_tomorrow(login, password, group): 
    "Get l/g/p and return string with schedule"
    try:
        request_groupname = str(group)
        request_data = {'group': request_groupname}    
        r = requests.post("https://miet.ru/schedule/data", data=request_data)
        jdata = json.loads(r.text) # json
        raw = parser.parse_week(login, password) # raw week string
        week = get_weektype(raw.split('/')[1]) 
        day = datetime.datetime.today().weekday() + 2 # tomorrow weekday
        classes = {}
        for x in jdata['Data']:
            if x['Day'] == day and x['DayNumber'] == week:
                classes[ x['Time']['Code'] ] = x
        answer = "Расписание на завтра\n"
        answer += "********************\n"
        for i in range(10):
            if i in classes:
                ftime = classes[i]['Time']['TimeFrom'].split('T')[1][0:5]
                ttime = classes[i]['Time']['TimeTo'].split('T')[1][0:5]
                answer += classes[i]['Time']['Time'] + ' с ' + ftime + ' до ' + ttime + '\n' 
                answer += classes[i]['Class']['Name'] + '\n' 
                answer += classes[i]['Class']['Teacher'] + '\n'
                answer += 'Аудитория ' + classes[i]['Room']['Name'] + '\n'
                answer += "********************\n"
        return answer 
    except:
        print('$$ SCH: /tomorrow error')
        return 'Ошибка при получении расписания на сегодня :('

def get_rasp_now(login, password, group): 
    "Get l/g/p and return string with current class"
    try:
        request_groupname = str(group)
        request_data = {'group': request_groupname}    
        r = requests.post("https://miet.ru/schedule/data", data=request_data)
        jdata = json.loads(r.text) # json
        raw = parser.parse_week(login, password) # raw week string
        week = get_weektype(raw.split('/')[1]) 
        day = datetime.datetime.today().weekday() + 1 # weekday
        time = (datetime.datetime.today().hour + 3)*60 + datetime.datetime.today().minute # now time +3 cz tz
        classes = {}
        for x in jdata['Data']:
            if x['Day'] == day and x['DayNumber'] == week:
                classes[ x['Time']['Code'] ] = x
        answer = '$'
        for i in range(10):
            if i in classes:
                rawftime = classes[i]['Time']['TimeFrom'].split('T')[1].split(':') # from time
                rawttime = classes[i]['Time']['TimeTo'].split('T')[1].split(':')   # to time
                ftime = int(rawftime[0])*60 + int(rawftime[1])
                ttime = int(rawttime[0])*60 + int(rawttime[1])
                if (ftime <= time) and (time <= ttime):
                    answer = 'Сейчас ' + classes[i]['Time']['Time'] + '\n' 
                    answer += classes[i]['Class']['Name'] + '\n' 
                    answer += classes[i]['Class']['Teacher'] + '\n'
                    answer += 'Аудитория ' + classes[i]['Room']['Name'] + '\n'
                    answer += rawftime[0] + ':' + rawftime[1] + ' - ' + rawttime[0] + ':' + rawttime[1] + '\n'
        if answer != '$':
            return answer
        else: 
            return 'Сейчас нет пар!'
            print('$$ SCH: /now log - ')
            print(ftime, time, ttime)
    except:
        print('$$ SCH: /now error')
        return 'Ошибка!'

