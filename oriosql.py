################## REQUIREMENTS ###################
# CODE STYLE:
# only functions, global vars and test lines
# comments in function header like ''' example '''
# try/except for all funcs
# ANSWER:
# one string var with \t, \n and so on
# if error occured return string like:
# $$ PARSER: super fatal error while parsing!
###################################################

# all important data is replaced by <wrapper>

import pymysql.cursors

def get_user(user_id, pincode):
    ''' ERRROR = ['NoLogin', 'NoPassword', 'NoGroup'] '''    
    answer = ['NoLogin', 'NoPassword', 'NoGroup']
    connection = pymysql.connect(host=<host>,user=<user>,password=<password>,
                                 db=<db name>,charset=<chatset>,cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            print('$$ DB: Looking for user...')
            sql = <request>
            cursor.execute(sql, (str(user_id),))
            result = cursor.fetchone()
            answer[0] = result['login']
            answer[1] = cipher(result['password'], pincode)[7:]
            answer[2] = result['grp']
            print('$$ DB: Return userdata...')
    except:
        print('$$ DB: USER NOT FOUND!')
    finally:
        connection.close()
    return answer

def delete_user(user_id, pincode):
    ''' delete user from db, return False if failed ''' 
    check_list = get_user(user_id, pincode)
    if check_list == ['NoLogin', 'NoPassword', 'NoGroup']:
        return False
    else:
        print('$$ DB: Found! Deleting user ' + str(user_id))
        connection = pymysql.connect(host=<host>,user=<user>,password=<password>,
                                     db=<db name>,charset=<chatset>,cursorclass=pymysql.cursors.DictCursor)
        try:
            with connection.cursor() as cursor:
                delete_sql = <delete_query>
                cursor.execute(delete_sql, (str(user_id),))
            connection.commit()
            print('$$ DB: User was deleted from db.')
            return True
        except:
            print('$$ DB: User was NOT deleted from db!')
            return False
        finally:
            connection.close()   

def add_to_table(user_id, user_login, user_password, pincode, group):
    enpass = encrypt(user_login + user_password, pincode)
    sql = <add_query>
    connection = pymysql.connect(host=<host>,user=<user>,password=<password>,
                                 db=<db name>,charset=<chatset>,cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            cursor.execute(sql, (str(user_id), str(user_login), str(enpass), str(group)))
        connection.commit()
        print('$$ DB: User ' + str(user_id) + ' was added!')
    except:
        print('$$ DB: FAILED to add user!')
    finally:
        connection.close()      
